public class Board {
    private Die die1;
    private Die die2;
    private boolean[] tiles;

    public Board() {
        this.tiles = new boolean[12];
        this.die1 = new Die();
        this.die2 = new Die();

    }

    public String toString() {
        String str = "";
        for (int i = 0; i < this.tiles.length; i++) {

            if (tiles[i]) {
                str += "X ";
            }

            else {
                str += (i + 1 + " ");
            }

            str += " ";
        }
        return str;
    }

    public boolean playATurn() {
        int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
        die1.roll();
        die2.roll();
        System.out.println(die1);
        System.out.println(die2);
        if (!tiles[sumOfDice - 1]) {
            tiles[sumOfDice - 1] = true;
            System.out.println("Closing tile equal to sum: " + (sumOfDice));
            return false;
        } else if (!tiles[die1.getFaceValue() - 1]) {
            tiles[die1.getFaceValue() - 1] = true;
            System.out.println("Closing tile equal to die1: " + (die1.getFaceValue()));
            return false;
        } else if (!tiles[die2.getFaceValue() - 1]) {
            tiles[die2.getFaceValue() - 1] = true;
            System.out.println("Closing tile equal to die2: " + (die2.getFaceValue()));
            return false;
        } else {
            System.out.println("All the tiles for these values are already shut");
            return true;
        }
    }

}
